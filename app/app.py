#! /usr/bin/env python3
from flask import Flask
from flask.ext.admin import Admin

app= Flask(__name__,static_url_path='/static')
admin = Admin(app, name='admin module')





app.debug=True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=True

from flask.ext.login import LoginManager
login_manager = LoginManager(app)


#protection contre CSRF ( cross site request forgery)
import uuid
app.config['SECRET_KEY']=str(uuid.uuid4())
#app.config['SECRET_KEY']="baa12166-40aa-4308-82df-2c49fe49b6dd"
# ajout boostrap

app.config['BOOTSTRAP_SERVE_LOCAL'] = True
from flask.ext.bootstrap import Bootstrap
Bootstrap(app)



from flask.ext.script import Manager
manager= Manager(app)

import os.path


def mkpath(p):
	return os.path.normpath(
	os.path.join(
		os.path.dirname(__file__),
		p))


from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI']=('sqlite:///'+mkpath('../myapp.db'))

db = SQLAlchemy(app)
