from app.app import app,admin
from flask import render_template
from .models import *
import random
import os.path

app.add_url_rule('/pub/2A/WebServeur/rockdb/<path:filename>', endpoint='rockdb', view_func=app.send_static_file)

@app.route('/images/<path:filename>')
def getImage(filename):

	if(not filename=="404.png"):
		return redirect(url_for("static",filename="images/images/"+filename))
	else:
		return redirect(url_for("static",filename="images/other_image/404.png"))

@app.route('/')
@app.route('/index')
def index():
	liste = get_sample(1200)
	one=liste[random.randint(0,99)]
	two=liste[random.randint(100,199)]
	three=liste[random.randint(200,299)]
	foor=liste[random.randint(300,399)]
	five=liste[random.randint(400,499)]
	six=liste[random.randint(500,599)]
	ratings = []
	ratings.append([get_rating_by_id_moy(one.id), get_posts_byAlbum_count(one.id)])
	ratings.append([get_rating_by_id_moy(two.id), get_posts_byAlbum_count(two.id)])
	ratings.append([get_rating_by_id_moy(three.id), get_posts_byAlbum_count(three.id)])
	ratings.append([get_rating_by_id_moy(foor.id), get_posts_byAlbum_count(foor.id)])
	ratings.append([get_rating_by_id_moy(five.id), get_posts_byAlbum_count(five.id)])
	ratings.append([get_rating_by_id_moy(six.id), get_posts_byAlbum_count(six.id)])
	return render_template("index.html",
	title="Home", ratings=ratings, one=one, two=two, three=three, foor=foor, five=five, six=six,
	car1=liste[random.randint(600,699)], car2=liste[random.randint(700,799)], car3=liste[random.randint(800,899)],
	car4=liste[random.randint(900,999)], car5=liste[random.randint(1000,1099)], car6=liste[random.randint(1100,1199)])




from flask import abort
@app.route('/list', defaults={'page': 1})
@app.route('/list/page/<int:page>')
def list(page):
	page2=page
	if page2==1:
		page2=0
	PER_PAGE=9
	count = Album.query.count()
	listeL =get_albums_for_page(page2-1, PER_PAGE)
	data = listeL
	if not data and page != 1:
		abort(404)
	#pagination = Pagination(page, PER_PAGE, count)
	pagination = Pagination(Album.query.all(),page, PER_PAGE,count,data)
	posts={}
	for albi in data:
		posts[albi.id]=[get_rating_by_id_moy(albi.id),get_posts_byAlbum_count(albi.id)]
		print(posts[albi.id])
	return render_template("list.html", title="list", pagination=pagination, posts=posts, subtitle="Our full list")

@app.route('/recent', defaults={'page': 1})
@app.route('/recent/page/<int:page>')
def recent(page):
	page2=page
	if page2==1:
		page2=0
	PER_PAGE=9
	count = Album.query.filter(Album.releaseYear>((datetime.datetime.now().year)-2)).count()
	listeAlb = get_albums_for_page_recent(page2-1, PER_PAGE)
	data = listeAlb
	if not data and page !=1:
		abort(404)
	pagination = Pagination(get_latest(),page, PER_PAGE,count,data)
	posts={}
	for albi in data:
		posts[albi.id]=[get_rating_by_id_moy(albi.id),get_posts_byAlbum_count(albi.id)]
		print(posts[albi.id])
	return render_template("list.html", title="recent", pagination=pagination,posts=posts, subtitle="All from two years ago")

@app.route('/top50')
def top50():
	liste50album=get_first50album_byRating()
	posts={}
	for albi in liste50album:
		posts[albi.id]=[get_rating_by_id_moy(albi.id),get_posts_byAlbum_count(albi.id)]
		print(posts[albi.id])
	return render_template("top50.html", title="top50", liste50=liste50album, posts=posts)

#Ajout de authorForm
from flask.ext.wtf import Form
from wtforms import StringField , HiddenField, RadioField


from wtforms . validators import DataRequired

#pour le login
from wtforms import PasswordField, validators
from .models import User
from hashlib import sha256


class LoginForm(Form):
    username = StringField('Username', [validators.Required(),validators.Length(min=4, max=25)])
    password = PasswordField('Password', [validators.Required(),validators.Length(min=4, max=25)])
    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        rv = Form.validate(self)
        if not rv:
            print('erreur Form.validate')
            return False

        user = User.query.get(self.username.data.lower())
        if user is None:
            self.username.errors.append('Unknown username')
            return False

        if not user.check_password(self.password.data):
            self.password.errors.append('Invalid password')
            return False

        return True



def redirect_url(default='index'):
	"""redirection page précédente """
	return request.args.get('next') or \
           request.referrer or \
           url_for(default)

#vue login
from flask.ext.login import login_user, current_user,login_required
from flask import request
from flask import flash, redirect, url_for, session, render_template
from flask.ext.login import  current_user,logout_user
@app.route("/login", methods=("GET","POST",))
def login():
      f = LoginForm()



      print("validé")
      if f.validate_on_submit():
          print("après validé submit")
          user = User.query.get(f.username.data.lower())
          if f.validate():
              if user:
                  flash(u'Successfully logged in as %s' % user.username)
                  login_user(user)
                  return redirect('index')

      return render_template('login.html', form=f)

@app.route("/settings")
@login_required
def settings():
    pass





@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(redirect_url())
from wtforms import  BooleanField

class SignupForm(Form):
  username = StringField("username",  [validators.Required("Please enter your username.")])
  email = StringField("Email",  [validators.Required("Please enter your email address."), validators.Email("Please enter your email address.")])
  password = PasswordField('Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
  confirm = PasswordField('Repeat Password')
  accept_tos = BooleanField('I accept the <a href="/about/tos" target="blank">Terms of Service</a> and <a href="/about/privacy-policy" target="blank">Privacy Notice</a> (updated Dec 22, 2015)</label>', [validators.Required()])

  def __init__(self, *args, **kwargs):
    Form.__init__(self, *args, **kwargs)

  def validate(self):
    if not Form.validate(self):
      return False

    user = User.query.filter_by(email = self.email.data.lower()).first()
    if user:
      self.email.errors.append("That email is already taken")
      return False
    else:
      return True


class PostForm(Form):
  username=HiddenField ('username')
  date=HiddenField ('date')
  album_id = HiddenField ('album_id')

  title=StringField("title",  [validators.Required("Please enter your title.")])
  rating = RadioField("rating", choices=[(0,"nul"),(1,"un peu moins nul"),(2,"bof"),(3,"pas mal"),(4,"bien"),(5,"extra")],coerce=int)
  comment = StringField("comment",  [validators.Required("Please enter your comment .")])
@app.route('/item', methods=['GET', 'POST'])
@app.route('/item/<int:id>', methods=['GET', 'POST'])
def item(id):

	alb = get_album_byID(id)
	username=None
	if current_user.is_authenticated:
	    username = current_user.username

	f = PostForm(username= username ,album_id=alb.id )
	posts=get_posts_byAlbum(id)
	moy=get_rating_by_id_moy(id)
	return render_template("item.html", title="item", alb=alb,form=f,posts=posts,moy=moy)

@app.route('/post', methods=['GET', 'POST'])
@login_required
def postMessage():
	f=PostForm()
	if current_user.is_authenticated and f.validate():
		alb = get_album_byID(f.album_id.data)
		print("hello")
		username=f.username.data
		user=User.query.get(username)
		#print(username)
		album_id=f.album_id.data
		title=f.title.data
		rating=f.rating.data
		comment=f.comment.data
		#print(username)
		post=Post(user=user,album_id=album_id,title=title,rating=rating,comment=comment,album=alb)
		db.session.add(post)
		db.session.commit()

		#return    redirect(url_for('item',id=alb.id))
		return redirect(url_for('item',id=alb.id))
	else:
		return redirect(url_for('item',id=alb.id))



from flask.ext.sqlalchemy import Pagination
#from app.models import Pagination
from flask import redirect
from app.models import get_albums_for_page,Album
PER_PAGE = 20
from math import ceil

@app.route('/signup', methods=['GET', 'POST'])
def signup():
  form = SignupForm()

  if request.method == 'POST':
    if form.validate() == False:
      print("non validé")
      return render_template('signup.html', form=form)
    else:
      print("ici")
      from hashlib import sha256
      m=sha256()
      m.update(form.password.data.encode())
      passwd = m.hexdigest()
      newuser = User(username=form.username.data,email=form.email.data, password=passwd)
      print("Username : ", newuser.username)
      print("email : ", newuser.email)
      print("password : ", newuser.password)
      db.session.add(newuser)
      db.session.commit()

      return redirect(url_for('login'))

  elif request.method == 'GET':
    return render_template('signup.html', form=form)




from flask.ext.admin import Admin, BaseView, expose
from flask.ext.admin.contrib.sqla import ModelView

class MyView(BaseView):
    @expose('/')
    def index(self):
        return self.render('adminIndex.html')

    def is_accessible(self):
        return current_user.is_authenticated


admin.add_view(ModelView(User, db.session,name='Users', endpoint='users', category='Manage'))
admin.add_view(ModelView(Post, db.session,name='Posts', endpoint='posts', category='Manage'))
admin.add_view(ModelView(Album, db.session,name='Albums', endpoint='albums', category='Manage'))
