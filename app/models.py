from .app import manager,db
from sqlalchemy import Table, Column, Integer, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.inspection import inspect
from sqlalchemy import PrimaryKeyConstraint
import time
import datetime

class Album(db.Model):
    __tablename__ = 'album'
    id=db.Column(db.Integer,primary_key=True)
    title=db.Column(db.String(100))
    releaseYear=db.Column(db.Integer)
    img=db.Column(db.String(100))
    composer_id = db.Column(db.Integer,db.ForeignKey("composer.id"),default=None)
    composer=db.relationship("Composer",backref=db.backref("albums"))
    artist_id = db.Column(db.Integer,db.ForeignKey("artist.id"))
    artist=db.relationship("Artist",backref=db.backref("albums"))
    genres = relationship("Genre",
                    secondary="association",
                    backref="albums")#AJouter dynamics
    def __repr__(self):
        return "title : %s" % (self.title)

class Post(db.Model):
    __tablename__ = 'post'
    id=db.Column(db.Integer,primary_key=True)
    title=db.Column(db.String(100),default='untitled')
    date=db.Column(db.Date, default=datetime.datetime.utcnow)
    updated_at = Column(db.Date, onupdate=datetime.datetime.utcnow)
    rating=db.Column(db.Integer,default=0)
    comment=Column(db.String(500), default='')
    user_id = db.Column(db.Integer,db.ForeignKey("user.username"),default=None)
    user=db.relationship("User",backref=db.backref("posts"))
    album_id = db.Column(db.Integer,db.ForeignKey("album.id"),default=None)
    album=db.relationship("Album",backref=db.backref("posts"))
    def __repr__(self):
        return "id : %d" % (self.id)

class Artist(db.Model):
	id= db.Column(db.Integer,primary_key=True)
	name = db.Column(db.String(100),nullable=False)
	def __repr__(self):
	       return "id : (%d) name : %s" % (self.id,self.name)


association_table = Table('association', db.metadata,
    Column('album_id', db.Integer, ForeignKey('album.id')),
    Column('genre_nom', db.String, ForeignKey('genre.nom'))
)


class Genre(db.Model):
    __tablename__ = 'genre'
    nom = db.Column(db.String(100),primary_key=True)
    def __repr__(self):
        return "genre : %s" % (self.genre)

class Composer(db.Model):
	id= db.Column(db.Integer,primary_key=True)
	name = db.Column(db.String(100),nullable=False)
	def __repr__(self):
	       return "id %d :  %s" % (self.id,self.name)


from flask.ext.login import UserMixin

class User(db.Model, UserMixin):
    __tablename__ = 'user'
    username=  db.Column(db.String(50),primary_key=True)
    password = db.Column(db.String(64))
    email = db.Column(db.String(120), unique=True)
    def __repr__(self):
        return "username : %s" % (self.username)

    def get_id(self):
        return self.username

    def check_password(self,unencrypted_password):
        from hashlib import sha256
        m=sha256()
        m.update(unencrypted_password.encode())
        passwd = m.hexdigest()
        return passwd == self.password

from .app import login_manager
@login_manager.user_loader
def load_user(username):
    return User.query.get(username)

def get_sample(number):
	return Album.query.filter(Album.img!=None).limit(number).all()

def get_albums_for_page(page, PER_PAGE):
    return Album.query.limit(PER_PAGE).offset(page*PER_PAGE).all()
def get_album_byID(id):
    return Album.query.get(id)


def get_first50posts_byRating():
    return Post.query.order_by(Post.rating).limit(50).all()

def get_first50album_byRating():
    liste50post = get_first50posts_byRating()
    list50album=[]
    for post in liste50post:
        list50album.append(post.album)
    return list50album

def get_posts_byAlbum(id):
    return Album.query.get(id).posts

def get_albums_for_page_recent(page, PER_PAGE):
    return Album.query.filter(Album.releaseYear>((datetime.datetime.now().year)-2)).limit(PER_PAGE).offset(page*PER_PAGE).all()

def get_latest():
    return Album.query.filter(Album.releaseYear>((datetime.datetime.now().year)-2)).all()

def get_rating_by_id_moy(id):
    ratings = get_posts_byAlbum(id)
    res = 0
    i=0
    for p in ratings:
        res += p.rating
        i+=1
    if i!=0:
        res=res/i
    else:
        res = 0
    return int(res)

def get_posts_byAlbum_count(id):
    return len(get_posts_byAlbum(id))

#_____________________________________________________________________
from .app import login_manager

@login_manager.user_loader
def load_user(username):
	return User.query.get(username)


#_____________________________________________________________________
