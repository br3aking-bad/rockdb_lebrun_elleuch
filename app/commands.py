from .app import manager,db
from .models import *
import time


@manager.command
def loaddb(filename):
	'''creates the tables and populates them with data.'''
	db.create_all()
	import yaml
	bands=yaml.load(open(filename, encoding="utf8"))

	artists= {}
	parents={}
	album={}
	genres={}
	for b in bands:
		s=b["by"]
		if(s !=None):
			if s not in artists.keys():

				c=Artist(name=s)
				artists[s]=c
				db.session.add(c)
				artists[s]=c
				db.session.flush()

			c=b["parent"]
		if (c!=None):
			if c not in parents.keys():
				a=Composer(name=c)
				db.session.add(a)
				parents[c]=a
				db.session.flush()
		artistId=-1
		try:
			artistId=artists[b["by"]].id
		except:
			artistId=None

		composerId=-1
		try:
			composerId=parents[b["parent"]].id
		except:
			composerId=None
		al=b["title"]

		if(al not in album.keys()):
			images=b["img"]
			if(images==None or images==""):
				images="404.png"

			o=Album(
			title=b["title"],
			releaseYear=b["releaseYear"],
			img=images,
			composer_id=composerId,
			artist_id=artistId,
			)
			album[al]=o
			db.session.add(o)

			db.session.flush()

		listeGenre=b["genre"]
		unAlbum=album[b["title"]]
		for genre in listeGenre:
			if genre!=None:
				if  genre.lower() not in genres.keys():
					genres[genre.lower()]={}
					gen=Genre(nom=genre.lower())
					genres[genre.lower()]["genre"]=gen
					genres[genre.lower()]["albums"]=[]
					db.session.add(gen)
					db.session.flush()

				if  not unAlbum.id in genres[genre.lower()]["albums"]:

					genres[genre.lower()]["albums"].append(unAlbum.id)
					unAlbum.genres.append(genres[genre.lower()]["genre"])


	db.session.commit()

#_____________________________________________________________________
'''

	for b in bands:
		o=Album(
		id=
	    title=
	    price =
	    releaseYear=
	    author =
	    url=
	    compositeur =
	    Genre =
		)
		b=b["by"]
		if a not in authors:
			o= Author(name=a)
			db.session.add(o)
			authors[a]=o
		db.session.commit()



		# deuxième passe: création de tout les livres

	for b in books:
		a=authors[b["author"]]
		o=Book(price =b["price"],
			title=b["title"],
			url=b["url"],
			img=b["img"],
			author_id=a.id)
		db.session.add(o)
	db.session.commit()
'''


@manager.command
def syncdb():
	'''Creates all missing tables. '''
	db.create_all()

@manager.command
def newuser(username, password):
	'''Ads a new user.'''
	from app.models import User
	from hashlib import sha256
	m=sha256()
	m.update(password.encode())
	u= User(username=username,password=m.hexdigest())
	db.session.add(u)
	db.session.commit()


@manager.command
def passwd(username, password):
	'''update password of  a user.'''
	from app.models import User
	from hashlib import sha256
	u=User.query.get(username)
	m=sha256()
	m.update(password.encode())
	u.password=m.hexdigest()
	db.session.commit()
